use axum::{routing::get, Router};
use dotenv::dotenv;
use sea_orm::{Database, DatabaseConnection};
use std::env;

pub mod entity;
pub mod handler;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    dotenv().ok();

    let db_url = env::var("DATABASE_URL").expect("not DATABASE_URL");

    print!("0.0.0.0:3000");

    let conn = Database::connect(db_url)
        .await
        .expect("database connection");
    // run it with hyper on localhost:3000
    axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
        .serve(router(conn).into_make_service())
        .await
        .unwrap();

    Ok(())
}

#[derive(Clone)]
pub struct Appstate {
    pub database: DatabaseConnection,
}

fn router(conn: DatabaseConnection) -> Router {
    Router::new()
        .route("/form/:id", get(handler::form::form))
        .with_state(conn)
}
