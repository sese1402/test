use axum::{
    extract::{self, Path, State},
    response::IntoResponse,
};
use sea_orm::{DatabaseConnection, EntityTrait, FromQueryResult, QuerySelect};
use serde::{Deserialize, Serialize};

use crate::entity::{user, field, form, prelude::*};

#[derive(Serialize, Deserialize, FromQueryResult, Debug)]
struct FormFrontend {
    #[serde(skip_serializing)]
    form_id: i32,
    form_name: String,
    form_owner: i32, // vorerst nur id
    #[serde(skip_serializing)]
    field_id: i32,
    field_type: String,
    field_name: String,
}

pub async fn form(
    State(ref conn): State<DatabaseConnection>,
    Path(formid): extract::Path<i32>,
) -> impl IntoResponse {
    let form_input: Vec<FormFrontend> = Form::find_by_id(formid)
        .left_join(Field)
        .left_join(User)
        .select_only()
        .column_as(form::Column::Id, "form_id")
        .column_as(form::Column::Name, "form_name")
        .column_as(user::Column::Name, "form_owner")
        .column_as(field::Column::Id, "field_id")
        .column_as(field::Column::Name, "field_name")
        .column_as(field::Column::Type, "field_type")
        .into_model::<FormFrontend>()
        .all(conn)
        .await
        .unwrap();

    serde_json::to_string(&form_input).unwrap()
}
