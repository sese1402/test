//! `SeaORM` Entity. Generated by sea-orm-codegen 0.11.3

pub mod prelude;

pub mod field;
pub mod filled_field;
pub mod form;
pub mod user;
