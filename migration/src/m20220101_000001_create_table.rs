use sea_orm_migration::{prelude::*, manager};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(User::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(User::Id)
                             .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(User::Name).string().not_null())
                    .to_owned(),
            ).await?;
        manager
            .create_table(
                Table::create()
                    .table(Form::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Form::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Form::Name).string().not_null())
                    .col(ColumnDef::new(Form::Owner).integer().not_null())
                    .to_owned(),
            ).await?;
        manager
            .create_table(
                Table::create()
                    .table(Field::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Field::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Field::Type).string().not_null())
                    .col(ColumnDef::new(Field::FormId).integer().not_null())
                    .col(ColumnDef::new(Field::Name).string().not_null())
                    .to_owned(),
            ).await?;
        manager
            .create_table(
                Table::create()
                    .table(FilledField::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(FilledField::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(FilledField::Content).string().not_null())
                    .col(ColumnDef::new(FilledField::FieldId).integer().not_null())
                    .col(ColumnDef::new(FilledField::User).integer().not_null())
                    .to_owned(),
            ).await?;
        manager
            .alter_table(
                Table::alter()
                    .table(Field::Table)
                    .add_foreign_key(
                        TableForeignKey::new()
                            .name("FK_Form_Field")
                            .from_tbl(Field::Table)
                            .from_col(Field::FormId)
                            .to_tbl(Form::Table)
                            .to_col(Form::Id),
                    )
                    .to_owned(),
            )
            .await?;
        manager
            .alter_table(
                Table::alter()
                    .table(FilledField::Table)
                    .add_foreign_key(
                        TableForeignKey::new()
                            .name("FK_Field_FilledField")
                            .from_tbl(FilledField::Table)
                            .from_col(FilledField::FieldId)
                            .to_tbl(Field::Table)
                            .to_col(Field::Id),
                    )
                    .to_owned(),
            )
            .await?;
         manager
            .alter_table(
                Table::alter()
                    .table(FilledField::Table)
                    .add_foreign_key(
                        TableForeignKey::new()
                            .name("FK_User_FilledField")
                            .from_tbl(FilledField::Table)
                            .from_col(FilledField::User)
                            .to_tbl(User::Table)
                            .to_col(User::Id),
                    )
                    .to_owned(),
            )
            .await?;
        manager
            .alter_table(
                Table::alter()
                    .table(Form::Table)
                    .add_foreign_key(
                        TableForeignKey::new()
                            .name("FK_User_Form")
                            .from_tbl(Form::Table)
                            .from_col(Form::Owner)
                            .to_tbl(User::Table)
                            .to_col(User::Id),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
    manager
        .drop_table(Table::drop().table(User::Table).to_owned())
        .await?;
    manager
        .drop_table(Table::drop().table(Form::Table).to_owned())
        .await?;
    manager
        .drop_table(Table::drop().table(Field::Table).to_owned())
        .await?;
    manager
        .drop_table(Table::drop().table(FilledField::Table).to_owned())
        .await?;
    manager
        .drop_foreign_key(ForeignKey::drop().name("FK_Form_Field").to_owned())
        .await?;
    manager
        .drop_foreign_key(ForeignKey::drop().name("FK_Field_FilledField").to_owned())
        .await?;
    manager
        .drop_foreign_key(ForeignKey::drop().name("FK_User_FilledField").to_owned())
        .await?;
    manager
        .drop_foreign_key(ForeignKey::drop().name("FK_User_Form").to_owned())
        .await
    


    
    
    }

}

/// Learn more at https://docs.rs/sea-query#iden
#[derive(Iden)]
enum User {
    Table,
    Id,
    Name,
}

#[derive(Iden)]
enum Form {
    Table, 
    Id,
    Name,
    Owner,
}

#[derive(Iden)]
enum Field {
    Table,
    Id,
    FormId,
    Name,
    Type,
}

#[derive(Iden)]
enum FilledField {
    Table,
    Id,
    FieldId,
    Content,
    User,
}
